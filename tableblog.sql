CREATE TABLE blog (
    blog_id uuid DEFAULT uuid_generate_v4 (),
title VARCHAR,
author_name VARCHAR,
content VARCHAR,
status SMALLINT NOT NULL,
header_image VARCHAR,
created_at TIMESTAMP WITH TIME ZONE NOT NULL,
modified_at TIMESTAMP WITH TIME ZONE,
deleted_at TIMESTAMP WITH TIME ZONE,
    PRIMARY KEY (blog_id)
);
