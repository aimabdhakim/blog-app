package handler

import (
	"context"

	"github.com/gin-gonic/gin"

	pb "apigateway/proto"

	"apigateway/config"
	"apigateway/utils"
)

type APIGatewayHandler interface {
	Route(*gin.Engine)
}

type apiGatewayServer struct {
	svcBlog pb.BlogAppService
}

func NewAPIGatewayServer(svcBlog pb.BlogAppService) *apiGatewayServer {
	return &apiGatewayServer{
		svcBlog: svcBlog,
	}
}

type apiGatewayImpl struct {
	cfg    *config.Config
	ctx    context.Context
	server *apiGatewayServer
}

func (_h *apiGatewayImpl) Route(r *gin.Engine) {
	var response = &utils.Response{}
	r.NoRoute(func(c *gin.Context) {
		response.SetNotFound(c)
	})
	r.NoMethod(func(c *gin.Context) {
		response.SetMethodNotAllowed(c)
	})

	// Blog Route
	r.GET("blog", _h.ListBlog)
	r.POST("/blog", _h.AddBlog)
	r.PUT("/blog", _h.UpdateBlog)
	blogRoute := r.Group("/blog")
	blogRoute.GET("/:blog_id", _h.DetailBlog)
	blogRoute.DELETE("/:blog_id", _h.DeleteBlog)
}

func NewAPIGatewayImpl(ctx context.Context, svc *apiGatewayServer, cfg *config.Config) APIGatewayHandler {
	return &apiGatewayImpl{
		ctx:    ctx,
		server: svc,
		cfg:    cfg,
	}
}
