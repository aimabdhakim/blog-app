package handler

import (
	pb "apigateway/proto"
	util "apigateway/utils"
	"utils"

	"github.com/gin-gonic/gin"
	"go-micro.dev/v4/logger"
)

func (_h *apiGatewayImpl) ListBlog(c *gin.Context) {
	var response = &util.Response{}
	type Param struct {
		Page       float32 `form:"page" json:"page" query:"page"`
		Keyword    string  `form:"keyword" json:"keyword" query:"keyword"`
		Sort       string  `form:"sort" json:"sort" query:"sort"`
		Limit      int     `form:"limit" json:"limit" query:"limit"`
		SortColumn string  `form:"sortcolumn" json:"sortcolumn" query:"sortcolumn"`
	}
	var param = &Param{}
	var request = &pb.GetBlogRequest{}

	if err := c.ShouldBindQuery(&param); err != nil {
		response.SetNotAcceptable(c, err.Error())
		return
	}

	pagination, _ := utils.TypeConverter[pb.PaginationRequest](param)
	if param != nil {
		request.Pagination = pagination
	}

	type Filter struct {
		Title      string `form:"title" json:"title" query:"title"`
		AuthorName string `form:"author_name" json:"author_name" query:"author_name"`
		Content    string `form:"content" json:"content" query:"content"`
		Status     string `form:"status" json:"status" query:"status"`
		BlogId     string `form:"blog_id" json:"blog_id" query:"blog_id"`
		CreatedAt  string `form:"created_at" json:"created_at" query:"created_at"`
		ModifiedAt string `form:"modified_at" json:"modified_at" query:"modified_at"`
	}

	var filter = &Filter{}
	if err := c.BindQuery(&filter); err != nil {
		response.SetNotAcceptable(c, err.Error())
		return
	}

	blogFilter, _ := utils.TypeConverter[pb.BlogFilter](filter)
	if filter != nil {
		request.Filter = blogFilter
	}

	resp, err := _h.server.svcBlog.ListBlog(_h.ctx, request)
	if err != nil {
		logger.Error("failed to get blog data, ", err)
		response.SetServerUnavailable(c)
		return
	}
	if !resp.Status {
		response.SetBadRequestError(c, resp.GetMsg())
		return
	}
	response.Message = resp.Msg
	response.Data = resp.Data
	response.SetOK(c)
}

func (_h *apiGatewayImpl) DetailBlog(c *gin.Context) {
	var response = &util.Response{}

	type Param struct {
		BlogID string `uri:"blog_id" binding:"required"`
	}
	var param = &Param{}
	if err := c.ShouldBindUri(&param); err != nil {
		logger.Error("error bind uri: ", err)
		response.SetBadRequestError(c, "you must specify blog id")
		return
	}

	var request = &pb.GetBlogRequest{
		BlogId: param.BlogID,
	}

	resp, err := _h.server.svcBlog.DetailBlog(_h.ctx, request)
	if err != nil {
		logger.Error(err)
		response.SetServerUnavailable(c)
		return
	}
	if !resp.Status {
		logger.Warn("fail to get detail blog: ", resp.Msg)
		response.SetNotFound(c)
		return
	}

	response.Data = resp.Data
	response.SetOK(c)
}

func (_h *apiGatewayImpl) AddBlog(c *gin.Context) {
	var response = &util.Response{}
	var err error

	var request = &pb.BlogRequest{}

	err = c.Bind(request)
	if err != nil {
		logger.Error("error to get request data, ", err)
		response.SetBadRequestError(c, "request not valid")
		return
	}

	resp, err := _h.server.svcBlog.AddBlog(_h.ctx, request)
	if err != nil {
		logger.Error("failed to add data, ", err)
		response.SetServerUnavailable(c)
		return
	}

	if !resp.Status {
		response.SetBadRequestError(c, resp.GetMsg())
		return
	}

	response.Message = resp.Msg
	response.Data = resp.Data
	response.SetOK(c)
}

func (_h *apiGatewayImpl) UpdateBlog(c *gin.Context) {
	var response = &util.Response{}
	var err error

	var request = &pb.BlogRequest{}

	err = c.Bind(request)
	if err != nil {
		logger.Error("error to get request data, ", err)
		response.SetBadRequestError(c, "request not valid")
		return
	}

	resp, err := _h.server.svcBlog.UpdateBlog(_h.ctx, request)
	if err != nil {
		logger.Error("failed to update data, ", err)
		response.SetServerUnavailable(c)
		return
	}

	if !resp.Status {
		response.SetBadRequestError(c, resp.GetMsg())
		return
	}

	response.Message = resp.Msg
	response.Data = resp.Data
	response.SetOK(c)
}

func (_h *apiGatewayImpl) DeleteBlog(c *gin.Context) {
	var response = &util.Response{}
	var err error

	type Param struct {
		BlogID string `uri:"blog_id" binding:"required"`
	}
	var param = &Param{}
	if err := c.ShouldBindUri(&param); err != nil {
		logger.Error("error bind uri: ", err)
		response.SetBadRequestError(c, "you must specify carousel id")
		return
	}

	var request = &pb.DetailBlogRequest{
		BlogId: param.BlogID,
	}

	resp, err := _h.server.svcBlog.DeleteBlog(_h.ctx, request)
	if err != nil {
		logger.Error("failed to delete data, ", err)
		response.SetServerUnavailable(c)
		return
	}

	if !resp.Status {
		response.SetBadRequestError(c, resp.GetMsg())
		return
	}

	response.Message = resp.Msg
	response.SetOK(c)
}
