package utils

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

var (
	ErrMsgNotFound            = "not found"
	ErrMsgMethodNotAllowed    = "method not allowed"
	ErrMsgBadRequest          = "bad request"
	ErrMsgForbidden           = "forbidden"
	ErrMsgUnauthorized        = "unauthorized"
	ErrMsgInternalServerError = "server error"
	ErrMsgServiceUnavailable  = "service unavailable"
	ErrMsgOperationTimeout    = "operation timeout"
	ErrMsgNotAcceptable       = "not acceptable"
	CodeNotFound              = 404
	CodeMethodNotAllowed      = 405
	CodeBadRequest            = 400
	CodeForbidden             = 403
	CodeUnauthorized          = 401
	CodeInternalServerError   = 500
	CodeServiceUnavailable    = 503
	CodeOperationTimeout      = 504
	CodeNotAcceptable         = 406
	CodeOK                    = 200
	CodeNoContent             = 204
)

type Response struct {
	Code    int    `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
	Error   string `json:"error,omitempty"`
	Data    any    `json:"data,omitempty"`
}

func (_r Response) SetNotFound(c *gin.Context) {
	_r.Code = CodeNotFound
	_r.Error = ErrMsgNotFound
	_r.Message = ""
	_r.Data = nil
	c.SecureJSON(http.StatusNotFound, _r)
}
func (_r Response) SetMethodNotAllowed(c *gin.Context) {
	_r.Code = CodeMethodNotAllowed
	_r.Error = ErrMsgMethodNotAllowed
	_r.Message = ""
	_r.Data = nil
	c.SecureJSON(http.StatusMethodNotAllowed, _r)
}
func (_r Response) SetOK(c *gin.Context) {
	_r.Code = CodeOK
	_r.Error = ""
	_r.Message = "ok"
	c.SecureJSON(http.StatusOK, _r)
}

func (_r *Response) SetNoContent(c *gin.Context) {
	_r.Code = CodeNoContent
	_r.Message = ""
	_r.Data = nil
	_r.Error = ""
	c.AbortWithStatus(http.StatusNoContent)
}

func (_r Response) SetAcceptence(c *gin.Context) {
	_r.Error = ""
	c.SecureJSON(http.StatusAccepted, _r)
}

func (_r Response) SetServerError(c *gin.Context) {
	_r.Code = CodeInternalServerError
	_r.Message = ""
	_r.Data = nil
	_r.Error = ErrMsgInternalServerError
	c.AbortWithStatusJSON(http.StatusInternalServerError, _r)
}

func (_r Response) SetServerUnavailable(c *gin.Context) {
	_r.Code = CodeServiceUnavailable
	_r.Message = ""
	_r.Data = nil
	_r.Error = ErrMsgServiceUnavailable
	c.AbortWithStatusJSON(http.StatusServiceUnavailable, _r)
}

func (_r Response) SetServerTimeOut(c *gin.Context) {
	_r.Code = CodeOperationTimeout
	_r.Message = ""
	_r.Data = nil
	_r.Error = ErrMsgOperationTimeout
	c.AbortWithStatusJSON(http.StatusGatewayTimeout, _r)
}

func (_r Response) SetBadRequestError(c *gin.Context, errMsg string) {
	_r.Code = CodeBadRequest
	_r.Message = ""
	_r.Data = nil
	_r.Error = errMsg
	c.AbortWithStatusJSON(http.StatusBadRequest, _r)
}

func (_r Response) SetUnauthorized(c *gin.Context, errMsg string) {
	_r.Code = CodeUnauthorized
	_r.Message = ""
	_r.Data = nil
	_r.Error = errMsg
	c.AbortWithStatusJSON(http.StatusUnauthorized, _r)
}

func (_r Response) SetForbidden(c *gin.Context, errMsg string) {
	_r.Code = CodeForbidden
	_r.Message = ""
	_r.Data = nil
	_r.Error = errMsg
	c.AbortWithStatusJSON(http.StatusForbidden, _r)
}

func (_r Response) SetNotAcceptable(c *gin.Context, errMsg string) {
	_r.Code = CodeNotAcceptable
	_r.Message = ""
	_r.Data = nil
	_r.Error = errMsg
	c.AbortWithStatusJSON(http.StatusNotAcceptable, _r)
}
