package middleware

import (
	"github.com/dvwright/xss-mw"
	"github.com/gin-gonic/gin"
	"go-micro.dev/v4/logger"
)

func ErrorHandler(c *gin.Context) {
	c.Next()
	for _, err := range c.Errors {
		logger.Error(err)
	}
}

func XssRemove(c *gin.Context) {
	var xssMdlwr xss.XssMw
	err := xssMdlwr.XssRemove(c)
	if err != nil {
		logger.Error(err)
		c.AbortWithStatusJSON(400, gin.H{"error": "invalid request"})
		return
	}
	c.Next()
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// c.Writer.Header().Set("Content-Encoding", "br")
		// c.Writer.Header().Set("Transfer-Encoding", "chunked")
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
