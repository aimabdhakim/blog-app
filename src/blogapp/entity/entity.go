package entity

import (
	pb "blogapp/proto"
	"strconv"
	"time"

	"gorm.io/gorm"
)

const (
	blogTableName string = "blog"
)

type Blog struct {
	BlogID      string         `gorm:"column:blog_id;default:uuid_generate_v4()"`
	Title       string         `gorm:"column:title"`
	AuthorName  string         `gorm:"column:author_name"`
	Content     string         `gorm:"column:content"`
	Status      int            `gorm:"column:status"`
	HeaderImage string         `gorm:"column:header_image"`
	CreatedAt   time.Time      `gorm:"column:created_at"`
	ModifiedAt  time.Time      `gorm:"column:modified_at"`
	DeletedAt   gorm.DeletedAt `gorm:"column:deleted_at"`
}

func (Blog) TableName() string {
	return blogTableName
}

func (_c *Blog) ToPB(in *pb.Blog) {
	in.BlogId = _c.BlogID
	in.Title = _c.Title
	in.AuthorName = _c.AuthorName
	in.Content = _c.Content
	in.Status = strconv.Itoa(_c.Status)
	in.HeaderImage = _c.HeaderImage
	in.CreatedAt = _c.CreatedAt.String()
	in.ModifiedAt = _c.ModifiedAt.String()
}
