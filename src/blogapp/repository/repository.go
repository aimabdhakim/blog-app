package repository

import (
	"blogapp/entity"
	"context"
	"utils"

	"gorm.io/gorm"
)

type BlogRepository interface {
	ListBlog(ctx context.Context, pagination *utils.Pagination, filter *utils.BlogFilter) ([]*entity.Blog, error)
	GetBlogByID(ctx context.Context, BlogID string) (*entity.Blog, error)
	AddBlog(ctx context.Context, Blog *entity.Blog) error
	UpdateBlog(ctx context.Context, Blog *entity.Blog) error
	DeleteBlog(ctx context.Context, BlogID string) error
	GetDB() *gorm.DB
}

type blogRepositoryImpl struct {
	db *gorm.DB
}

func (_r *blogRepositoryImpl) ListBlog(ctx context.Context, pagination *utils.Pagination, filter *utils.BlogFilter) (blogs []*entity.Blog, err error) {
	// get table name
	var blog = &entity.Blog{}

	var sqlWhere string
	if filter != nil {
		sqlWhere = utils.ProcessBlogFilter(filter)
	}

	var totalRows int64
	_r.db.WithContext(ctx).Model(&entity.Blog{}).Select("blog_id").Where(sqlWhere).Count(&totalRows)

	result := _r.db.WithContext(ctx).Table(blog.TableName()).Scopes(utils.Paginate(blogs, pagination, _r.db, pagination.SortColumn, totalRows))

	if sqlWhere != "" {
		result = result.Where(sqlWhere)
	}

	err = result.Find(&blogs).Error
	if len(blogs) == 0 {
		err = gorm.ErrRecordNotFound
		return
	}
	return
}

func (_r *blogRepositoryImpl) GetBlogByID(ctx context.Context, blogID string) (*entity.Blog, error) {
	blog := &entity.Blog{}
	result := _r.db.WithContext(ctx).Table(blog.TableName()).Where("blog_id = ?", blogID).First(&blog)
	if result.Error != nil {
		return nil, result.Error
	}
	return blog, nil
}

func (_r *blogRepositoryImpl) AddBlog(ctx context.Context, blog *entity.Blog) error {
	err := _r.db.WithContext(ctx).Table(blog.TableName()).Create(&blog).Error
	if err != nil {
		return err
	}
	return nil
}

func (_r *blogRepositoryImpl) UpdateBlog(ctx context.Context, blog *entity.Blog) error {
	err := _r.db.WithContext(ctx).Where("blog_id = ?", blog.BlogID).UpdateColumns(&blog).Error
	if err != nil {
		return err
	}
	return nil
}

func (_r *blogRepositoryImpl) DeleteBlog(ctx context.Context, blogID string) error {
	var blog = &entity.Blog{}

	if err := _r.db.WithContext(ctx).Where("blog_id = ?", blogID).Delete(blog).Error; err != nil {
		return err
	}
	return nil
}

func NewBlogRepositoryImpl(db *gorm.DB) BlogRepository {
	return &blogRepositoryImpl{
		db: db,
	}
}

func (_r *blogRepositoryImpl) GetDB() *gorm.DB {
	return _r.db
}
