package validation

import (
	"blogapp/entity"
	pb "blogapp/proto"
	"context"
	"errors"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type BlogRequest struct {
	Title       string `json:"title"`
	AuthorName  string `json:"author_name"`
	Content     string `json:"content"`
	Status      int    `json:"status"`
	HeaderImage string `json:"header_image"`
	BlogID      string `json:"blog_id"`
}

type DetailBlogRequest struct {
	UserID string `json:"user_id"`
	BlogID string `json:"blog_id"`
}

func (_r *BlogRequest) Load(in *pb.BlogRequest) {
	_r.Title = in.Title
	_r.AuthorName = in.AuthorName
	_r.Content = in.Content
	_r.Status = int(in.Status)
	_r.HeaderImage = in.HeaderImage
	_r.BlogID = in.BlogId
}

func (_r *BlogRequest) ToEntity(blog *entity.Blog) {
	blog.Title = _r.Title
	blog.AuthorName = _r.AuthorName
	blog.Content = _r.Content
	blog.Status = _r.Status
	blog.HeaderImage = _r.HeaderImage
}

func (request BlogRequest) Validate(ctx context.Context) error {
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Title, validation.Required),
		validation.Field(&request.AuthorName, validation.Required),
		validation.Field(&request.Content, validation.Required),
		validation.Field(&request.Status, validation.By(request.validateStatus)),
		validation.Field(&request.HeaderImage, validation.Required),
	)
	return err
}

func (_r BlogRequest) validateStatus(value any) error {
	valid := false
	booleanStatus0 := true
	booleanStatus1 := true

	if _r.Status != 0 {
		booleanStatus0 = false
	}

	if _r.Status != 1 {
		booleanStatus1 = false
	}

	if booleanStatus0 || booleanStatus1 {
		valid = !valid
	}

	if !valid {
		return errors.New("status cannot outside 0 or 1")
	}

	return nil
}

func (_r *DetailBlogRequest) Load(in *pb.DetailBlogRequest) {
	_r.BlogID = in.BlogId
}

func (request DetailBlogRequest) Validate(ctx context.Context) error {
	err := validation.ValidateStruct(&request,
		validation.Field(&request.BlogID, validation.Required),
	)

	return err
}
