package handler

import (
	"blogapp/service"
	"context"

	pb "blogapp/proto"
)

const (
	msgFailedToGetData string = "failed to get list of blog data"
	msgOk              string = "ok"
)

type BlogApp struct {
	service service.BlogService
}

func (_h *BlogApp) ListBlog(ctx context.Context, in *pb.GetBlogRequest, resp *pb.ListBlogResponse) error {
	resp.Status = false
	err := _h.service.ListBlog(ctx, in, resp)
	if err != nil {
		resp.Msg = err.Error()
	} else {
		resp.Status = !resp.Status
		resp.Msg = msgOk
	}

	if err == nil && resp.Data == nil {
		resp.Msg = msgFailedToGetData
	}
	return nil
}

func (_h *BlogApp) DetailBlog(ctx context.Context, in *pb.GetBlogRequest, resp *pb.DetailBlogResponse) error {
	resp.Status = false
	err := _h.service.DetailBlog(ctx, in, resp)
	if err != nil {
		resp.Msg = err.Error()
	} else {
		resp.Status = !resp.Status
		resp.Msg = msgOk
	}

	if err == nil && resp.Data == nil {
		resp.Msg = msgFailedToGetData
	}
	return nil
}

func (_h *BlogApp) AddBlog(ctx context.Context, in *pb.BlogRequest, resp *pb.DetailBlogResponse) error {
	resp.Status = false
	err := _h.service.AddBlog(ctx, in, resp)
	if err != nil {
		resp.Msg = err.Error()
	} else {
		resp.Status = !resp.Status
		resp.Msg = "ok"
	}
	return nil
}

func (_h *BlogApp) UpdateBlog(ctx context.Context, in *pb.BlogRequest, resp *pb.DetailBlogResponse) error {
	resp.Status = false
	err := _h.service.UpdateBlog(ctx, in, resp)
	if err != nil {
		resp.Msg = err.Error()
	} else {
		resp.Status = !resp.Status
		resp.Msg = "ok"
	}
	return nil
}

func (_h *BlogApp) DeleteBlog(ctx context.Context, in *pb.DetailBlogRequest, resp *pb.Response) error {
	resp.Status = false
	err := _h.service.DeleteBlog(ctx, in, resp)
	if err != nil {
		resp.Msg = err.Error()
	} else {
		resp.Status = !resp.Status
		resp.Msg = "ok"
	}
	return nil
}

func NewBlogAppHandler(service service.BlogService) pb.BlogAppHandler {
	return &BlogApp{
		service: service,
	}
}
