package service

import (
	"context"
	"errors"
	"time"
	"utils"

	"blogapp/entity"
	pb "blogapp/proto"
	"blogapp/repository"
	"blogapp/validation"

	"go-micro.dev/v4/logger"
	"gorm.io/gorm"
)

var (
	ErrRequiredBlogID      = errors.New("blog id is required")
	ErrBlogNotFound        = errors.New("blog not found")
	ErrFailToSaveBlog      = errors.New("fail to save blog")
	ErrFailToUpdateBlog    = errors.New("fail to update blog")
	ErrFailToDeleteBlog    = errors.New("fail to delete blog")
	ErrBlogIDCannotBeEmpty = errors.New("blog id cannot be empty")
)

const (
	ActionAddBlog    string = "add blog"
	ActionUpdateBlog string = "update blog"
	ActionDeleteBlog string = "delete blog"
)

type BlogService interface {
	ListBlog(ctx context.Context, in *pb.GetBlogRequest, resp *pb.ListBlogResponse) error
	DetailBlog(ctx context.Context, in *pb.GetBlogRequest, resp *pb.DetailBlogResponse) error
	AddBlog(ctx context.Context, in *pb.BlogRequest, resp *pb.DetailBlogResponse) error
	UpdateBlog(ctx context.Context, in *pb.BlogRequest, resp *pb.DetailBlogResponse) error
	DeleteBlog(ctx context.Context, in *pb.DetailBlogRequest, resp *pb.Response) error
}

type blogServiceImpl struct {
	repository repository.BlogRepository
}

func (_s *blogServiceImpl) ListBlog(ctx context.Context, in *pb.GetBlogRequest, resp *pb.ListBlogResponse) error {
	pagination := utils.NewPagination()
	pagination.Load(in.Pagination)
	if err := pagination.Validate(); err != nil {
		logger.Error(err)
		return err
	}

	blogFilter := utils.NewBlogFilter()
	blogFilter.Load(in.Filter)
	if err := blogFilter.Validate(); err != nil {
		logger.Error(err)
		return err
	}

	blogs, err := _s.repository.ListBlog(ctx, pagination, blogFilter)
	if err != nil {
		logger.Error(err)
		return err
	}

	var blogResp []*pb.Blog
	r := &utils.PaginationResponse{}
	r.Default(pagination)

	paginationResp, _ := utils.TypeConverter[pb.PaginationResponse](r)

	for _, v := range blogs {
		var blog = pb.Blog{}
		v.ToPB(&blog)
		blogResp = append(blogResp, &blog)
	}
	dataResp := &pb.BlogPaginationResponse{
		Result:     blogResp,
		Pagination: paginationResp,
	}
	resp.Data = dataResp

	return nil
}

func (_s *blogServiceImpl) DetailBlog(ctx context.Context, in *pb.GetBlogRequest, resp *pb.DetailBlogResponse) error {
	if in.GetBlogId() == "" {
		return ErrRequiredBlogID
	}

	blog, err := _s.repository.GetBlogByID(ctx, in.BlogId)
	if err != nil {
		logger.Error("blog not found")
		return ErrBlogNotFound
	}

	blogData := &pb.Blog{}
	blog.ToPB(blogData)

	resp.Data = blogData

	return err
}

func (_s *blogServiceImpl) AddBlog(ctx context.Context, in *pb.BlogRequest, resp *pb.DetailBlogResponse) error {
	var err error
	var request = &validation.BlogRequest{}
	var blog = &entity.Blog{}
	request.Load(in)
	if err := request.Validate(ctx); err != nil {
		return err
	}

	request.ToEntity(blog)
	blog.CreatedAt = time.Now()

	err = _s.repository.AddBlog(ctx, blog)
	if err != nil {
		logger.Error("failed to save blog, ", err)
		err = nil
		return ErrFailToSaveBlog
	}

	blogData := &pb.Blog{}
	blog.ToPB(blogData)
	resp.Data = blogData

	return err
}

func (_s *blogServiceImpl) UpdateBlog(ctx context.Context, in *pb.BlogRequest, resp *pb.DetailBlogResponse) error {
	var err error
	var request = &validation.BlogRequest{}
	var blog = &entity.Blog{}
	request.Load(in)
	if err := request.Validate(ctx); err != nil {
		return err
	}

	if request.BlogID == "" {
		logger.Error("blog id cannot empty")
		return ErrBlogIDCannotBeEmpty
	}

	tempBlog, err := _s.repository.GetBlogByID(ctx, request.BlogID)
	if tempBlog == nil || err == gorm.ErrRecordNotFound {
		logger.Error("blog not found, ", err)
		return ErrBlogNotFound
	}

	request.ToEntity(blog)
	blog.ModifiedAt = time.Now()
	blog.BlogID = request.BlogID

	err = _s.repository.UpdateBlog(ctx, blog)
	if err != nil {
		logger.Error("failed to update blog, ", err)
		err = nil
		return ErrFailToUpdateBlog
	}

	blogData := &pb.Blog{}
	blog.ToPB(blogData)
	resp.Data = blogData

	return err
}

func (_s *blogServiceImpl) DeleteBlog(ctx context.Context, in *pb.DetailBlogRequest, resp *pb.Response) error {
	var err error
	var request = &validation.DetailBlogRequest{}
	request.Load(in)
	if err = request.Validate(ctx); err != nil {
		return err
	}

	if request.BlogID == "" {
		logger.Error("blog id cannot empty")
		return ErrBlogIDCannotBeEmpty
	}

	tempBlog, err := _s.repository.GetBlogByID(ctx, request.BlogID)
	if tempBlog == nil || err == gorm.ErrRecordNotFound {
		logger.Error("blog not found, ", err)
		return ErrBlogNotFound
	}

	err = _s.repository.DeleteBlog(ctx, request.BlogID)
	if err != nil {
		logger.Error("failed to delete blog, ", err)
		err = nil
		return ErrFailToDeleteBlog
	}

	return nil
}

func NewBlogServiceImpl(repository repository.BlogRepository) BlogService {
	return &blogServiceImpl{
		repository: repository,
	}
}
