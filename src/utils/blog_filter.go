package utils

import (
	"errors"
	"strconv"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"go-micro.dev/v4/logger"
)

type BlogFilter struct {
	Title       string `json:"title"`
	Tag         string `json:"tag"`
	AuthorName  string `json:"author_name"`
	Content     string `json:"content"`
	Status      string `json:"status"`
	CategoryId  string `json:"category_id"`
	Featured    string `json:"featured"`
	UserId      string `json:"user_id"`
	BlogId      string `json:"blog_id"`
	CategoryNum []int
	CreatedAt   string `json:"created_at"`
	ModifiedAt  string `json:"modified_at"`
}

func (_r *BlogFilter) Load(in any) {
	blogFilter, err := TypeConverter[BlogFilter](in)
	if err != nil {
		logger.Error(err)
		return
	}
	var result = BlogFilter{}
	result = *blogFilter
	*_r = result
}

func (_r BlogFilter) Validate() error {
	return validation.ValidateStruct(&_r,
		validation.Field(&_r.Status, validation.In("0", "1")),
		validation.Field(&_r.CategoryId, validation.By(_r.validateCategoryID)),
		validation.Field(&_r.Featured, validation.In("0", "1")),
	)
}

func NewBlogFilter() *BlogFilter {
	return &BlogFilter{}
}

func ProcessBlogFilter(_r *BlogFilter) string {
	var sqlWhere string
	var tempSQL string
	if _r.AuthorName != "" {
		tempSQL = "LOWER(author_name) LIKE '%" + _r.AuthorName + "%'"
		if sqlWhere != "" {
			sqlWhere += " AND "
		}
		sqlWhere += tempSQL
	}

	if _r.BlogId != "" {
		tempSQL = "blog_id = '" + _r.BlogId + "'"
		if sqlWhere != "" {
			sqlWhere += " AND "
		}
		sqlWhere += tempSQL
	}

	if _r.CategoryId != "" {
		tempSQL = "category_id = " + _r.CategoryId
		if sqlWhere != "" {
			sqlWhere += " AND "
		}
		sqlWhere += tempSQL
	}

	if _r.Content != "" {
		tempSQL = "LOWER(content) LIKE '%" + _r.Content + "%'"
		if sqlWhere != "" {
			sqlWhere += " AND "
		}
		sqlWhere += tempSQL
	}

	if _r.Featured != "" {
		tempSQL = "featured = " + _r.Featured
		if sqlWhere != "" {
			sqlWhere += " AND "
		}
		sqlWhere += tempSQL
	}

	if _r.Status != "" {
		tempSQL = "status = " + _r.Status
		if sqlWhere != "" {
			sqlWhere += " AND "
		}
		sqlWhere += tempSQL
	}

	if _r.Tag != "" {
		tempSQL = "LOWER(tag) LIKE '%" + _r.Tag + "%'"
		if sqlWhere != "" {
			sqlWhere += " AND "
		}
		sqlWhere += tempSQL
	}

	if _r.Title != "" {
		tempSQL = "LOWER(title) LIKE '%" + _r.Title + "%'"
		if sqlWhere != "" {
			sqlWhere += " AND "
		}
		sqlWhere += tempSQL
	}

	if _r.UserId != "" {
		tempSQL = "user_id = '" + _r.UserId + "'"
		if sqlWhere != "" {
			sqlWhere += " AND "
		}
		sqlWhere += tempSQL
	}

	if _r.CreatedAt != "" {
		tempSQL = "date(created_at) = '" + _r.CreatedAt + "'"
		if sqlWhere != "" {
			sqlWhere += " AND "
		}
		sqlWhere += tempSQL
	}

	if _r.ModifiedAt != "" {
		tempSQL = "date(modified_at) '" + _r.ModifiedAt + "'"
		if sqlWhere != "" {
			sqlWhere += " AND "
		}
		sqlWhere += tempSQL
	}
	return sqlWhere
}

func (_r BlogFilter) validateCategoryID(value any) error {
	valid := false

	if _r.CategoryId == "" {
		valid = true
		return nil
	}

	categoryID, _ := strconv.Atoi(_r.CategoryId)

	for i := 0; i < len(_r.CategoryNum); i++ {
		if categoryID == _r.CategoryNum[i] {
			valid = true
			return nil
		}
	}

	if !valid {
		return errors.New("category_id is not registered")
	}

	return nil
}
