module utils

go 1.19

require (
	github.com/go-ozzo/ozzo-validation/v4 v4.3.0
	go-micro.dev/v4 v4.10.2
	gorm.io/gorm v1.25.3
)

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/stretchr/testify v1.8.3 // indirect
)
